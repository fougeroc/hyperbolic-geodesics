import sage.all
from template import *

# e = Experiment([0.11, 0.31, 0.36, 0.52, 0.77, 0.97], [0.53, 0.6, 0.71, 0.75, 0.89, 0.97])
# print e.hypergeometric_lyap_exp(nb_experiments=1, nb_vectors=6)
# print "[0.66732616323459948, 0.0096766678320557437, 0.00025668133370575317]"
# 
# f = Experiment([.32,.234,.467,.13],[.3,.1,.98,.536])
# print f.hypergeometric_lyap_exp(nb_experiments=1, nb_vectors=4)
# print "[0.17158470516894048, 0.017067166548209025]"
# print f.degree(1,verbose=False)

from random import random
a = [random() for _ in range(4)]
b = [random() for _ in range(4)]
f = Experiment(a, b)
print f
l = f.hypergeometric_lyap_exp()
print f.degree(0)
print f.degree(1)
print f.degree(2)
print f.degree(3)

def real_vhs(a, b, h):
    alpha, beta = range(h), range(h)
    for i in range(len(a)):
        alpha[i] = a[i]
        alpha[-i-1] = -a[i]
        beta[i] = b[i]
        beta[-i-1] = -b[i]
        if i == h-i: break
    print alpha, beta
    return Experiment(alpha, beta)

# from random import random
# a = [random() for _ in range(2)]
# b = [random() for _ in range(2)]
# g = real_vhs(a, b, 4)

def real_familly(x,y,z,t):
    assert(x>=0 and y>=0 and z>=0 and t >=0 and x+y+z+t<=1/2.)
    return(real_vhs([x,x+y+z+t],[x+y,x+y+z],4))

# x, y, z, t = 1/7.,1/8.,1/9.,1/10.
# print "coucou"
# print x, y, z, t
# e = real_familly(x,y,z,t)
# print e
# print sum(e._beta), sum(e._alpha)
# print e.gamma()
# l = e.hypergeometric_lyap_exp()
# print l
# print sum(l)
# print e.degree(2)
# print e.degree(1)
# print e.degree(0)
