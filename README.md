# Hyperbolic Geodesics

Simulates hyperbolic geodesics on a 3-pointed sphere and compute the correponding Lyapunov exponents for any flat bundle over it.


## Methods

According to the results of [Series85] we simulate a generic continued fraction expansion to obtain a generic coding of an hyperbolic geodesic with respect to Farey tesselation of Poincaré half plane. As two tiles of Farey tesselation form a fundamental domain for the 3-pointed hyperbolic sphere, we can simulate a generic geodesic on this sphere and keep track of the associated coding in the fundamental group.

The computation of Lyapunov exponents is rather classical. We evaluated the action of monodromy matrices on a generic vecotr and orthonormalizing it regularly we keep track of its growth in all directions.


## Hypergeometric equations

One interesting example of such flat bundles is given by some singular linear differential equations called hypergeometric differential equations.
A formula computed in [Fou19] is coded to compute the parabolic degrees of holomorphic subbundles of the induced flat bundle and their variation of Hogde structure.


# References

	- [Series85] -- Caroline Series, The modular surface and continued fractions. J. London Math. Soc. (2), vol. 31, (1985).
	- [Fou19] -- Charles Fougeron, Parabolic degrees and Lyapunov exponents for hypergeometric local systems. Exp. Math., (2019).
